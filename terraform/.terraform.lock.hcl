# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/bwoznicki/assert" {
  version     = "0.0.1"
  constraints = "0.0.1"
  hashes = [
    "h1:8TLHtofX1ZRdkobXeznFH5pHwLPyE0dGEHuDUbbhBCo=",
    "h1:PqhZ58g8EnSbGN2liQfr+ZlCi/6aDPUiHeugfIoQTbk=",
    "zh:04bcf081b46a1f5a49216355692dec53974d3d653276533eca8ff7c89f17e579",
    "zh:2afe1397d479bc56d34f9f2e567ce87d18e58a1972bdae608d16d05f2987790c",
    "zh:2f2ab5a72610087e07851aba02d7aee80d9e5bcd8fe3d5bfc86c975a9b78fe91",
    "zh:48d342de102a8beb7dcc0ef7b40c6624a384bb4114be53d03d77c34347e5c19a",
    "zh:49462b0d126d00a87be0ea4fdc90a6784ffcb53f07f5041440a1b85107a5a4f4",
    "zh:6144955c31175579fe27effa9f57f1fb6aec362c312b21cfb8beea31f873cc4b",
    "zh:b122781035a17f0304e5c7ac16e0e9375af0d2de7818b2920bd02564ae332f15",
    "zh:bfb29f53b26b6634e1f1ff6ad36df14a8b2cac4c240682e24ef23947e89ae185",
    "zh:e5e966851e07816cb909da443d9298b2b74708ad24479ded82635bcf9e17b4af",
    "zh:e6060146383e14670939ff85437c4cd4f46b29deaf092317805f37b53637b9cb",
    "zh:ea17b139dd612a5d805bbe8cbdcee7e327cd42b6cbe6b6d4988bc19fd388dfae",
    "zh:ebac227dfceda29b0f7c7d6d8dc798e07b8165279caef29effda0613cef5317d",
  ]
}

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.26.0"
  constraints = "~> 3.0"
  hashes = [
    "h1:LI3kK89an79DiIw0lAD8DkvjveAnWRzyeoGjBqp7EX8=",
    "zh:0fa412e209ba0cc75302a5e426596e4edf542d7a60ec901ded6ef855f9ee3d6b",
    "zh:3da71fee8ac124b8b6b019de3de4e3652fd829748a4c5979fd7b5f6e53be262f",
    "zh:4613ab92ce56cc5c2b2ac0eeee3b3a9af10c22ed5033a30f1ee0d19e6d6f0f55",
    "zh:479cb852e36d2f1eb80495123e837e9002ec98a810390d42e1a8e8f24f8e5c60",
    "zh:757cc10c41fcbd2685510e866c006bca95245bc41770a51d0845417b1ab64d47",
    "zh:78efac7d71b464d4e3e0717c1739a1d6dd4c9129277262ed10d7a0632d1b750d",
    "zh:828ba3c266ba58105e4fd8460e88dc5747b23e0dbcaa72ba324b31a36e91ba45",
    "zh:8f5f58d2aaa2060f361b2935dcb351b3ee2512bd1cfc0c6466e2ef52ad5c2012",
    "zh:9dc781745e80956be8d24c00e2782437210087710a1009baf29375b35c8af2aa",
    "zh:a1a06305f43dabc92c5bdec446fec5304ae245ab63e177ad5e04036f06b2c1df",
    "zh:a78d82fed749c97a5217240471d98de91ed9e7bf61bee0c4bfa9e4bdee638e4f",
    "zh:ccd9460b90818eba1515de19f6b3b589fb50964ba6583b117bdd28c4495578d1",
    "zh:eb73ac80f26c14034dcd686bc5f16e6eb5a4d18cb307bfe356ee3a221ae19900",
    "zh:f08e9110cfc7d8771c2ac2698f6817b81173d9eedbb227f1513e9bd3db786064",
  ]
}

provider "registry.terraform.io/hashicorp/archive" {
  version     = "2.2.0"
  constraints = "~> 2.0"
  hashes = [
    "h1:01FetUdrgOAnGkdnYJkmXFCzO3GIzYNkI+CS0TzbYlw=",
    "h1:2K5LQkuWRS2YN1/YoNaHn9MAzjuTX8Gaqy6i8Mbfv8Y=",
    "h1:4ImYvZXF2QkxmaAvWLWoCG6fPVR3xvzPyLcRC7j1ZEU=",
    "h1:62mVchC1L6vOo5QS9uUf52uu0emsMM+LsPQJ1BEaTms=",
    "h1:B+jc0hGbYzYq3fTCUb+uEZgs31GAF6/V2AxdqXUB7Qo=",
    "h1:CIWi5G6ob7p2wWoThRQbOB8AbmFlCzp7Ka81hR3cVp0=",
    "h1:P9Sjz/FPXain0vGhNrJB7FVhniTYr7H47QnQ1xlWMuY=",
    "h1:Po27P/6bWtscWGSbsKfuf5npdNcqdBHuxwddJ/lOCIw=",
    "h1:Rxkd7mWSvHMLppeKeW6+7BxWGP0h4xZdfb5sd4pGQq8=",
    "h1:iXz2pH03GPpGk4kDTi9U/qunWjj+Wk3ch+w5SDJMKIc=",
    "h1:mZPzA0bba3fHD0Ht01Qu1r1x8uKHGJbKK1/CJn11vFI=",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "4.36.1"
  constraints = "~> 4.0"
  hashes = [
    "h1:sN3HFvBwuCn+ipD9Ti5OnBJ+V9CzUXviJ2py6tiCK6Q=",
    "zh:19b16047b4f15e9b8538a2b925f1e860463984eed7d9bd78e870f3e884e827a7",
    "zh:3c0db06a9a14b05a77f3fe1fc029a5fb153f4966964790ca8e71ecc3427d83f5",
    "zh:3c7407a8229005e07bc274cbae6e3a464c441a88810bfc6eceb2414678fd08ae",
    "zh:3d96fa82c037fafbd3e7f4edc1de32afb029416650f6e392c39182fc74a9e03a",
    "zh:8f4f540c5f63d847c4b802ca84d148bb6275a3b0723deb09bf933a4800bc7209",
    "zh:9802cb77472d6bcf24c196ce2ca6d02fac9db91558536325fec85f955b71a8a4",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:a263352433878c89832c2e38f4fd56cf96ae9969c13b5c710d5ba043cbd95743",
    "zh:aca7954a5f458ceb14bf0c04c961c4e1e9706bf3b854a1e90a97d0b20f0fe6d3",
    "zh:d78f400332e87a97cce2e080db9d01beb01f38f5402514a6705d6b8167e7730d",
    "zh:e14bdc49be1d8b7d2543d5c58078c84b76051085e8e6715a895dcfe6034b6098",
    "zh:f2e400b88c8de170bb5027922226da1e9a6614c03f2a6756c15c3b930c2f460c",
  ]
}

provider "registry.terraform.io/hashicorp/external" {
  version     = "2.2.2"
  constraints = "~> 2.0"
  hashes = [
    "h1:/Qsdu8SIXbfANKJFs1UTAfvcomJUalOd3uDZvj3jixA=",
    "h1:5907Z/VOLV770KacxmmM1jG4H1l6ruXAvKPQAB0sMNc=",
    "h1:BKQ5f5ijzeyBSnUr+j0wUi+bYv6KBQVQNDXNRVEcfJE=",
    "h1:VUkgcWvCliS0HO4kt7oEQhFD2gcx/59XpwMqxfCU1kE=",
    "h1:WbgNwc9bn0I6NjGGPxzlzZKtuCTY2NDlE0a2zcgI1nQ=",
    "h1:XikEI1c0/y0fb421oaXKfvQPHz9QSLh/ZLW9W2ibuw8=",
    "h1:dMIJtnI19wjOXLuvNjNlL1g7jDjotlCAN8gx7OyztOM=",
    "h1:e7RpnZ2PbJEEPnfsg7V0FNwbfSk0/Z3FdrLsXINBmDY=",
    "h1:jb8vLKmZGfq6cz8l+MxsctUzGpxAKPaGxKT0WV8Hz6U=",
    "h1:uUPId756h2t0DpXRBx7wb8e+bnfLnQbSqShSsb84dRY=",
    "h1:wRojmwSjn61w4sp+0Xiz1Crs08lPg8UTmcY1WZBEGFw=",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version     = "3.1.1"
  constraints = "~> 3.0"
  hashes = [
    "h1:++qmvEmm1We5awLCnYOR1AaO56YlaN5EkrE6Gu8wJeg=",
    "h1:1J3nqAREzuaLE7x98LEELCCaMV6BRiawHSg9MmFvfQo=",
    "h1:71sNUDvmiJcijsvfXpiLCz0lXIBSsEJjMxljt7hxMhw=",
    "h1:C6d0vOG3Q2O8iKb6PHLdoAnCUuVpXykYM8txguXzeVo=",
    "h1:CfThxGv5+osX7FL8mhC0yPj9jZt56PaadHeemx5R+k4=",
    "h1:MEdgcR43dgp/SL6Y0z4GBmtqSQ6reZ7JtJhKMbzfKc0=",
    "h1:Pctug/s/2Hg5FJqjYcTM0kPyx3AoYK1MpRWO0T9V2ns=",
    "h1:YKDUZ/sQcFVUoHBH/ndHQU9B8VrnyZREgMWiIYjMaXs=",
    "h1:YvH6gTaQzGdNv+SKTZujU1O0bO+Pw6vJHOPhqgN8XNs=",
    "h1:ZD4wyZ0KJzt5s2mD0xD7paJlVONNicLvZKdgtezz02I=",
    "h1:kLrErio1jzW8ahFQ7EUcFy0pXMjJMkVEb+Lry3/QD6E=",
  ]
}
